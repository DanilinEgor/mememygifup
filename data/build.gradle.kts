plugins {
    id("java")
    kotlin("jvm")
}

dependencies {
    api(kotlin("stdlib", "1.3.31"))
    api("javax.inject:javax.inject:1")
    api("io.reactivex.rxjava2:rxjava:2.2.8")
    api("com.jakewharton.timber:timber-android:5.0.0-SNAPSHOT")
}