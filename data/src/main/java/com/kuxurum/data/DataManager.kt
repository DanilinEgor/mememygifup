package com.kuxurum.data

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataManager @Inject constructor(
    private val remoteSource: RemoteSource,
    private val localSource: LocalSource
) {
    fun getTrendingGifs(offset: Int): Single<GifListResult> {
        return remoteSource
            .getTrendingGifs(offset)
            .doOnSuccess { localSource.saveGifs(it.gifs) }
            .onErrorReturn { GifListResult(error = it as Exception) }
            .subscribeOn(Schedulers.io())
    }

    fun getGifById(id: String): Single<GifInfoResult> {
        return localSource.getGifById(id)
            .filter { it.isNotEmpty() }
            .map { GifInfoResult(gif = it.first()) }
            .switchIfEmpty(Single.defer { remoteSource.getGifById(id) })
            .filter { it.gif != null }
            .onErrorReturn { GifInfoResult(error = it as Exception) }
            .switchIfEmpty(Single.just(GifInfoResult(error = NoGifFoundException())))
            .subscribeOn(Schedulers.io())
    }
}

interface RemoteSource {
    fun getTrendingGifs(offset: Int): Single<GifListResult>
    fun getGifById(id: String): Single<GifInfoResult>
}

interface LocalSource {
    fun saveGifs(gifs: List<EntityGif>)
    fun getGifById(id: String): Single<List<EntityGif>>
}