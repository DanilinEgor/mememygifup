package com.kuxurum.data

data class EntityGif(
    val id: String,
    val url: String,
    val urlDownsampled: String,
    val w: Int,
    val h: Int,
    val profileUrl: String?,
    val username: String?,
    val displayName: String?,
    val twitter: String?
)

data class GifListResult(
    val gifs: List<EntityGif> = emptyList(),
    val hasNext: Boolean = false,
    val error: Exception? = null
)

data class GifInfoResult(
    val gif: EntityGif? = null,
    val error: Exception? = null
)

class NoGifFoundException : Exception()
class TooManyRequestsException : Exception()