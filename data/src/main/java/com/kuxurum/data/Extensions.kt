package com.kuxurum.data

import timber.log.Timber
import timber.log.assert
import timber.log.debug
import timber.log.error
import timber.log.info
import timber.log.verbose
import timber.log.warn

fun Timber.v(message: String) {
    Timber.verbose { message }
}

fun Timber.d(message: String) {
    Timber.debug { message }
}

fun Timber.a(message: String) {
    Timber.assert { message }
}

// to be able to pass function reference to .subscribe()
fun Timber.e(throwable: Throwable) {
    Timber.e(throwable, "")
}

fun Timber.e(throwable: Throwable, message: String = "") {
    Timber.error(throwable) { message }
}

fun Timber.w(message: String) {
    Timber.warn { message }
}

fun Timber.i(message: String) {
    Timber.info { message }
}