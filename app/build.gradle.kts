plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
}

android {
    compileSdkVersion(28)
    defaultConfig {
        applicationId = "com.kuxurum.mememygifup"
        minSdkVersion(21)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    implementation(project(":data"))
    implementation(project(":local"))
    implementation(project(":remote"))

    implementation("androidx.appcompat:appcompat:1.0.2")
    implementation("androidx.core:core-ktx:1.0.2")
    implementation("androidx.constraintlayout:constraintlayout:1.1.3")
    implementation("com.google.android.material:material:1.0.0")

    implementation("com.jakewharton:butterknife:10.1.0")
    kapt("com.jakewharton:butterknife-compiler:10.1.0")

    implementation("com.google.dagger:dagger:2.23")
    implementation("com.google.dagger:dagger-android:2.23")
    implementation("com.google.dagger:dagger-android-support:2.23")
    kapt("com.google.dagger:dagger-compiler:2.23")
    kapt("com.google.dagger:dagger-android-processor:2.23")

    implementation("io.reactivex.rxjava2:rxandroid:2.1.1")

    implementation("com.github.bumptech.glide:glide:4.9.0")
}
