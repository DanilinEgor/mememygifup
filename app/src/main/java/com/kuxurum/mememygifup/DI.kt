package com.kuxurum.mememygifup

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import com.kuxurum.data.LocalSource
import com.kuxurum.data.RemoteSource
import com.kuxurum.local.LocalDataManager
import com.kuxurum.mememygifup.gifinfo.GifInfoFragment
import com.kuxurum.mememygifup.gifinfo.GifInfoPresenter
import com.kuxurum.mememygifup.giflist.GifListFragment
import com.kuxurum.mememygifup.giflist.GifListPresenter
import com.kuxurum.remote.RemoteDataManager
import dagger.Binds
import dagger.Component
import dagger.MapKey
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton
import kotlin.reflect.KClass

@Singleton
@Component(modules = [AppModule::class, ViewModelProviderModule::class, DataModule::class, AndroidInjectionModule::class])
interface AppComponent {
    fun inject(app: App)
}

@Module
abstract class AppModule {
    @ContributesAndroidInjector
    abstract fun contributeGifListFragment(): GifListFragment

    @ContributesAndroidInjector
    abstract fun contributeGifInfoFragment(): GifInfoFragment

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(GifListPresenter::class)
    internal abstract fun gifListPresenter(viewModel: GifListPresenter): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GifInfoPresenter::class)
    internal abstract fun gifInfoPresenter(viewModel: GifInfoPresenter): ViewModel
}

@Module
object ViewModelProviderModule {
    @JvmStatic
    @Provides
    @Singleton
    fun viewModelProvider(factory: ViewModelProvider.Factory): ViewModelProvider {
        return ViewModelProvider(ViewModelStore(), factory)
    }
}

@Module
class DataModule(private val context: Context) {
    @Provides
    @Singleton
    fun remoteDataSource(): RemoteSource {
        return RemoteDataManager()
    }

    @Provides
    @Singleton
    fun localDataSource(): LocalSource {
        return LocalDataManager(context)
    }
}

@MapKey
@Target(AnnotationTarget.FUNCTION)
annotation class ViewModelKey(
    val value: KClass<out ViewModel>
)

@Suppress("UNCHECKED_CAST")
@Singleton
class ViewModelFactory @Inject constructor(
    private val viewModels: MutableMap<Class<out ViewModel>, Provider<ViewModel>>
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val viewModelProvider = viewModels[modelClass]
            ?: throw IllegalArgumentException("model class $modelClass not found")
        return viewModelProvider.get() as T
    }
}