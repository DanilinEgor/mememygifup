package com.kuxurum.mememygifup.giflist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.kuxurum.data.EntityGif
import com.kuxurum.mememygifup.R
import com.kuxurum.mememygifup.R.drawable
import com.kuxurum.mememygifup.R.layout

class GifListAdapter : RecyclerView.Adapter<ViewHolder>() {
    private val asyncDiffer =
        AsyncListDiffer<EntityGif>(this, object : DiffUtil.ItemCallback<EntityGif>() {
            override fun areItemsTheSame(oldItem: EntityGif, newItem: EntityGif): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: EntityGif, newItem: EntityGif): Boolean {
                return oldItem.urlDownsampled == newItem.urlDownsampled
            }
        })

    private val items
        get() = asyncDiffer.currentList

    var hasNextPage = true
    var onClickListener: ((id: String) -> Unit)? = null

    init {
        setHasStableIds(true)
    }

    fun replaceGifs(newItems: List<EntityGif>) {
        asyncDiffer.submitList(newItems)
    }

    override fun getItemId(position: Int): Long {
        return getItem(position)?.id?.hashCode()?.toLong() ?: -2L
    }

    override fun getItemCount(): Int {
        return items.size + if (hasNextPage) 1 else 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasNextPage && position == items.size) layout.item_loading_more else layout.item_gif
    }

    private fun getItem(position: Int) = items.getOrNull(position)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            layout.item_gif -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(layout.item_gif, parent, false)
                GifViewHolder(view).apply {
                    image.setOnClickListener {
                        adapterPosition.takeIf { it != NO_POSITION }?.let {
                            getItem(it)?.let { onClickListener?.invoke(it.id) }
                        }
                    }
                }
            }
            layout.item_loading_more -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(layout.item_loading_more, parent, false)
                LoadingMoreViewHolder(view)
            }
            else -> TODO()
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (getItemViewType(position) == layout.item_gif) {
            holder as GifViewHolder
            getItem(position)?.let { holder.bind(it) }
        }
    }
}

class GifViewHolder(itemView: View) : ViewHolder(itemView) {
    @BindView(R.id.iv_gif)
    lateinit var image: ImageView
    @BindView(R.id.tv_gif_id)
    lateinit var idText: TextView

    init {
        ButterKnife.bind(this, itemView)
    }

    fun bind(entityGif: EntityGif) {
        image.updateLayoutParams<LayoutParams> {
            height = (width * entityGif.h * 1f / entityGif.w).toInt()
        }
        Glide.with(image)
            .load(entityGif.urlDownsampled)
            .placeholder(drawable.placeholder)
            .into(image)
        idText.text = entityGif.id
    }
}

class LoadingMoreViewHolder(itemView: View) : ViewHolder(itemView) {}