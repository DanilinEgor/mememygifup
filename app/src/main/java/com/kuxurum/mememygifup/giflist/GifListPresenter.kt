package com.kuxurum.mememygifup.giflist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kuxurum.data.DataManager
import com.kuxurum.data.EntityGif
import com.kuxurum.data.GifListResult
import com.kuxurum.data.e
import com.kuxurum.mememygifup.Coordinator
import com.kuxurum.mememygifup.giflist.GifListPartialState.NextPageLoaded
import com.kuxurum.mememygifup.giflist.GifListPartialState.NextPageLoading
import com.kuxurum.mememygifup.giflist.GifListPartialState.RefreshLoaded
import com.kuxurum.mememygifup.giflist.GifListPartialState.RefreshLoading
import com.kuxurum.mememygifup.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.PublishProcessor
import timber.log.Timber
import javax.inject.Inject

class GifListPresenter @Inject constructor(
    private val dataManager: DataManager,
    private val coordinator: Coordinator
) : ViewModel() {
    private val dataSubscriptions = CompositeDisposable()

    private val partialStateProcessor = BehaviorProcessor.create<GifListPartialState>()
    private val _stateLiveData = MutableLiveData<GifListState>()
    val stateLiveData: LiveData<GifListState>
        get() = _stateLiveData
    private val pullToRefreshProcessor = PublishProcessor.create<Unit>()
    private val nextPageProcessor = PublishProcessor.create<Unit>()

    override fun onCleared() {
        super.onCleared()
        dataSubscriptions.clear()
    }

    init {
        dataSubscriptions += pullToRefreshProcessor
            .filter { _stateLiveData.value?.isRefreshing == false }
            .flatMap {
                dataManager
                    .getTrendingGifs(0)
                    .toFlowable()
                    .observeOn(AndroidSchedulers.mainThread())
                    .map<GifListPartialState> { RefreshLoaded(it) }
                    .startWith(RefreshLoading)
            }
            .subscribe(partialStateProcessor::onNext, Timber::e)

        dataSubscriptions += nextPageProcessor
            .filter { _stateLiveData.value?.isLoadingNextPage == false }
            .flatMap {
                dataManager
                    .getTrendingGifs(_stateLiveData.value?.gifs?.size ?: 0)
                    .toFlowable()
                    .observeOn(AndroidSchedulers.mainThread())
                    .map<GifListPartialState> { NextPageLoaded(it) }
                    .startWith(NextPageLoading)
            }
            .subscribe(partialStateProcessor::onNext, Timber::e)

        dataSubscriptions += partialStateProcessor
            .scan(
                GifListState(
                    gifs = emptyList(),
                    isLoadingNextPage = false,
                    isRefreshing = false,
                    hasNextPage = true,
                    error = null
                ),
                this::stateReducer
            )
            .subscribe({ _stateLiveData.value = it }, Timber::e)

        refresh()
    }

    fun refresh() {
        pullToRefreshProcessor.offer(Unit)
    }

    fun loadNextPage() {
        nextPageProcessor.offer(Unit)
    }

    private fun stateReducer(
        accumulator: GifListState,
        newState: GifListPartialState
    ): GifListState {
        when (newState) {
            is NextPageLoaded -> {
                return accumulator.copy(
                    gifs = (accumulator.gifs + newState.gifListResult.gifs).distinctBy { it.id },
                    isLoadingNextPage = false,
                    isRefreshing = false,
                    hasNextPage = newState.gifListResult.hasNext,
                    error = newState.gifListResult.error
                )
            }
            is NextPageLoading -> {
                return accumulator.copy(
                    isLoadingNextPage = true
                )
            }
            is RefreshLoaded -> {
                return accumulator.copy(
                    gifs = newState.gifListResult.gifs,
                    isLoadingNextPage = false,
                    isRefreshing = false,
                    error = newState.gifListResult.error
                )
            }
            is RefreshLoading -> {
                return accumulator.copy(
                    isRefreshing = true
                )
            }
        }
    }

    fun onGifClicked(id: String) {
        coordinator.goToGifInfo(id)
    }
}

sealed class GifListPartialState {
    class NextPageLoaded(val gifListResult: GifListResult) : GifListPartialState()
    object NextPageLoading : GifListPartialState()

    class RefreshLoaded(val gifListResult: GifListResult) : GifListPartialState()
    object RefreshLoading : GifListPartialState()
}

data class GifListState(
    val gifs: List<EntityGif>,
    val isLoadingNextPage: Boolean,
    val isRefreshing: Boolean,
    val hasNextPage: Boolean,
    val error: Exception?
)