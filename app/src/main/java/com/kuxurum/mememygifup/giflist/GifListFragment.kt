package com.kuxurum.mememygifup.giflist

import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager.VERTICAL
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import butterknife.BindDimen
import butterknife.BindView
import butterknife.ButterKnife
import com.google.android.material.snackbar.Snackbar
import com.kuxurum.data.TooManyRequestsException
import com.kuxurum.data.e
import com.kuxurum.mememygifup.R
import com.kuxurum.mememygifup.R.layout
import com.kuxurum.mememygifup.R.string
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

class GifListFragment : Fragment() {
    @Inject
    lateinit var viewModelProvider: ViewModelProvider
    lateinit var presenter: GifListPresenter

    @BindView(R.id.rv_gifs_list)
    lateinit var recyclerView: RecyclerView
    @BindView(R.id.srl_gifs_list)
    lateinit var swipeRefreshLayout: SwipeRefreshLayout

    @BindDimen(R.dimen.gif_width)
    @JvmField
    var gifW = 0
    @BindDimen(R.dimen.gif_padding)
    @JvmField
    var gifPadding = 0

    private val adapter = GifListAdapter()
    private var errorSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)

        presenter = viewModelProvider[GifListPresenter::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(layout.fragment_gifs_list, container, false)
        ButterKnife.bind(this, view)

        val columnW = gifW + 2 * gifPadding
        val spanCount = Resources.getSystem().displayMetrics.widthPixels / columnW
        val positions = IntArray(spanCount)

        recyclerView.layoutManager = StaggeredGridLayoutManager(spanCount, VERTICAL)
        recyclerView.adapter = adapter.apply {
            onClickListener = { presenter.onGifClicked(it) }
        }
        recyclerView.addOnScrollListener(object : OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                (recyclerView.layoutManager as StaggeredGridLayoutManager)
                    .findLastVisibleItemPositions(positions)

                if (positions.any { it > adapter.itemCount - spanCount * 3 }) {
                    presenter.loadNextPage()
                }
            }
        })

        swipeRefreshLayout.setOnRefreshListener { presenter.refresh() }

        return view
    }

    override fun onStart() {
        super.onStart()
        presenter.stateLiveData.observe(this, Observer(::render))
    }

    private fun render(state: GifListState) {
        adapter.hasNextPage = state.hasNextPage && !state.isRefreshing
        recyclerView.post { adapter.notifyDataSetChanged() }
        adapter.replaceGifs(state.gifs)
        swipeRefreshLayout.isRefreshing = state.isRefreshing
        state.error?.let { e ->
            Timber.e(e)
            errorSnackbar = Snackbar.make(
                swipeRefreshLayout,
                when (e) {
                    is TooManyRequestsException -> string.too_many_requests
                    is IOException -> string.network_error
                    else -> string.default_error
                },
                Snackbar.LENGTH_INDEFINITE
            )
            errorSnackbar?.show()
        } ?: run { errorSnackbar?.dismiss() }
    }
}