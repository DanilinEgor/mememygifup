package com.kuxurum.mememygifup

import androidx.fragment.app.FragmentActivity
import com.kuxurum.mememygifup.gifinfo.GifInfoFragment
import com.kuxurum.mememygifup.giflist.GifListFragment
import java.lang.ref.WeakReference
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Coordinator @Inject constructor(private val navigator: Navigator) {
    fun start() {
        navigator.openGifList()
    }

    // Go to Info screen from main list
    fun goToGifInfo(id: String) {
        navigator.goToGifInfo(id)
    }

    // Just open Info screen (without adding to back stack)
    fun openGifInfo(id: String) {
        navigator.openGifInfo(id)
    }
}

@Singleton
class Navigator @Inject constructor() {
    private var activity: WeakReference<FragmentActivity>? = null

    fun install(activity: FragmentActivity) {
        this.activity = WeakReference(activity)
    }

    fun openGifList() {
        activity
            ?.get()
            ?.supportFragmentManager
            ?.beginTransaction()
            ?.replace(R.id.fragment_container, GifListFragment())
            ?.commit()
    }

    fun openGifInfo(id: String) {
        activity
            ?.get()
            ?.supportFragmentManager
            ?.beginTransaction()
            ?.replace(R.id.fragment_container, GifInfoFragment.instantiate(id))
            ?.commit()
    }

    fun goToGifInfo(id: String) {
        activity
            ?.get()
            ?.supportFragmentManager
            ?.beginTransaction()
            ?.addToBackStack("GifInfo_$id")
            ?.replace(R.id.fragment_container, GifInfoFragment.instantiate(id))
            ?.commit()
    }

    fun clear() {
        activity = null
    }
}