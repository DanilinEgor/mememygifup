package com.kuxurum.mememygifup

import android.content.UriMatcher
import android.net.Uri
import android.os.Bundle
import dagger.Reusable
import javax.inject.Inject

@Reusable
class UriHandler @Inject constructor(private val coordinator: Coordinator) {
    private val uriMatcher = UriMatcher(UriMatcher.NO_MATCH).apply {
        addURI("giphy.com", "/gifs/*", CODE_GIF)
        addURI("gifs", "/*", CODE_GIF)
    }

    fun handleUri(savedInstanceState: Bundle?, uri: Uri?) {
        uri?.let { process(it) } ?: run {
            if (savedInstanceState == null) {
                coordinator.start()
            }
        }
    }

    private fun process(uri: Uri?): Uri? {
        uri ?: return null
        if (uriMatcher.match(uri) == CODE_GIF) {
            coordinator.openGifInfo(uri.pathSegments[if (uri.authority == "gifs") 0 else 1])
            return uri
        }
        return null
    }

    companion object {
        private const val CODE_GIF = 1
    }
}