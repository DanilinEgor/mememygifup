package com.kuxurum.mememygifup

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var uriHandler: UriHandler
    @Inject
    lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigator.install(this)
        uriHandler.handleUri(savedInstanceState, intent.data)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        uriHandler.handleUri(null, intent?.data)
    }

    override fun onDestroy() {
        navigator.clear()
        super.onDestroy()
    }
}
