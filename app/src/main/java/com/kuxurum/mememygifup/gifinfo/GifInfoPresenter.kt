package com.kuxurum.mememygifup.gifinfo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kuxurum.data.DataManager
import com.kuxurum.data.EntityGif
import com.kuxurum.data.GifInfoResult
import com.kuxurum.data.e
import com.kuxurum.mememygifup.gifinfo.GifInfoPartialState.GifLoaded
import com.kuxurum.mememygifup.gifinfo.GifInfoPartialState.GifLoading
import com.kuxurum.mememygifup.plusAssign
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.PublishProcessor
import timber.log.Timber
import javax.inject.Inject

class GifInfoPresenter @Inject constructor(private val dataManager: DataManager) : ViewModel() {
    private val dataSubscriptions = CompositeDisposable()
    private val partialStateProcessor = BehaviorProcessor.create<GifInfoPartialState>()
    private val idProcessor = PublishProcessor.create<String>()
    private val retryProcessor = PublishProcessor.create<Unit>()
    private val _stateLiveData = MutableLiveData<GifInfoState>()
    val stateLiveData: LiveData<GifInfoState>
        get() = _stateLiveData

    override fun onCleared() {
        super.onCleared()
        dataSubscriptions.clear()
    }

    init {
        dataSubscriptions += partialStateProcessor
            .map(this::mapper)
            .startWith(
                GifInfoState(
                    gif = null,
                    isLoading = true,
                    error = null
                )
            )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _stateLiveData.value = it
            }, Timber::e)

        dataSubscriptions += Flowable.combineLatest(
            idProcessor,
            retryProcessor.startWith(Unit),
            BiFunction<String, Unit, String> { id, _ -> id })
            .switchMapSingle { dataManager.getGifById(it) }
            .map { GifLoaded(it) }
            .subscribe({ partialStateProcessor.offer(it) }, Timber::e)
    }

    fun loadData(id: String) {
        idProcessor.offer(id)
    }

    fun retry() {
        partialStateProcessor.offer(GifLoading)
        retryProcessor.offer(Unit)
    }

    private fun mapper(newState: GifInfoPartialState): GifInfoState {
        when (newState) {
            is GifLoaded -> {
                return GifInfoState(
                    gif = newState.result.gif,
                    isLoading = false,
                    error = newState.result.error
                )
            }
            is GifLoading -> {
                return GifInfoState(
                    gif = null,
                    isLoading = true,
                    error = null
                )
            }
        }
    }
}

sealed class GifInfoPartialState {
    class GifLoaded(val result: GifInfoResult) : GifInfoPartialState()
    object GifLoading : GifInfoPartialState()
}

data class GifInfoState(
    val gif: EntityGif?,
    val isLoading: Boolean,
    val error: Exception?
)