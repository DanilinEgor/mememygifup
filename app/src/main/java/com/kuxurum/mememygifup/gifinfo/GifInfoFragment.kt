package com.kuxurum.mememygifup.gifinfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.kuxurum.data.NoGifFoundException
import com.kuxurum.data.TooManyRequestsException
import com.kuxurum.data.e
import com.kuxurum.mememygifup.R
import com.kuxurum.mememygifup.R.drawable
import com.kuxurum.mememygifup.R.layout
import com.kuxurum.mememygifup.R.string
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

class GifInfoFragment : Fragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var presenter: GifInfoPresenter

    @BindView(R.id.layout_gif_info_progress)
    lateinit var progressLayout: View

    @BindView(R.id.layout_gif_info_retry)
    lateinit var retryLayout: View
    @BindView(R.id.tv_gif_info_error)
    lateinit var errorText: TextView

    @BindView(R.id.layout_gif_info_info)
    lateinit var infoLayout: View
    @BindView(R.id.iv_gif_info)
    lateinit var image: ImageView
    @BindView(R.id.tv_gif_info_username)
    lateinit var usernameText: TextView
    @BindView(R.id.tv_gif_info_name)
    lateinit var nameText: TextView
    @BindView(R.id.tv_gif_info_twitter)
    lateinit var twitterText: TextView
    @BindView(R.id.tv_gif_info_profile_url)
    lateinit var urlText: TextView

    @BindView(R.id.toolbar_gif_info)
    lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        presenter =
            ViewModelProvider(ViewModelStore(), viewModelFactory)[GifInfoPresenter::class.java]

        arguments?.getString(KEY_ID)?.let { presenter.loadData(it) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(layout.fragment_gif_info, container, false)
        ButterKnife.bind(this, view)
        toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
        return view
    }

    override fun onStart() {
        super.onStart()
        presenter.stateLiveData.observe(this, Observer(::render))
    }

    private fun render(state: GifInfoState) {
        if (state.isLoading) {
            progressLayout.isVisible = true
            retryLayout.isVisible = false
            infoLayout.isVisible = false
            return
        }
        state.error?.let { e ->
            Timber.e(e)
            progressLayout.isVisible = false
            retryLayout.isVisible = true
            infoLayout.isVisible = false

            errorText.setText(
                when (e) {
                    is NoGifFoundException -> string.could_not_find_gif
                    is TooManyRequestsException -> string.too_many_requests
                    is IOException -> string.network_error
                    else -> string.default_error
                }
            )
        }

        state.gif?.let {
            progressLayout.isVisible = false
            retryLayout.isVisible = false
            infoLayout.isVisible = true

            Glide.with(this@GifInfoFragment)
                .load(it.url)
                .placeholder(drawable.placeholder)
                .into(image)

            nameText.text = getString(string.info_name, it.displayName?.let { "@$it" } ?: "-")
            usernameText.text = getString(string.info_username, it.username ?: "-")
            twitterText.text = getString(string.info_twitter, it.twitter ?: "-")
            urlText.text = it.profileUrl
        }
    }

    @OnClick(R.id.btn_gif_info_retry)
    fun onClick(v: View) {
        when (v.id) {
            R.id.btn_gif_info_retry -> presenter.retry()
        }
    }

    companion object {
        private const val KEY_ID = "KEY_ID"
        fun instantiate(id: String): GifInfoFragment {
            return GifInfoFragment().apply {
                arguments = Bundle().apply { putString(KEY_ID, id) }
            }
        }
    }
}