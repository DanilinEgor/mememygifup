package com.kuxurum.remote

import com.kuxurum.data.GifInfoResult
import com.kuxurum.data.GifListResult
import com.kuxurum.data.NoGifFoundException
import com.kuxurum.data.RemoteSource
import com.kuxurum.data.TooManyRequestsException
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import timber.log.verbose
import java.io.IOException
import java.util.concurrent.TimeUnit.SECONDS

class RemoteDataManager : RemoteSource {
    private val service: GiphyApiService
    private val moshi: Moshi

    init {
        val client = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor { Timber.verbose { it } }.setLevel(BODY))
            .callTimeout(30, SECONDS)
            .connectTimeout(30, SECONDS)
            .readTimeout(30, SECONDS)
            .writeTimeout(30, SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.giphy.com")
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()

        service = retrofit.create(GiphyApiService::class.java)
        moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    }

    override fun getTrendingGifs(offset: Int): Single<GifListResult> {
        return service.getTrending(API_KEY, offset)
            .map { it.data }
            .map { it.map { MapperGifObject.map(it) } }
            .map { GifListResult(it, it.size == 25) }
            .onErrorReturn { e ->
                GifListResult(
                    error = when (e) {
                        is HttpException -> when (e.code()) {
                            429 -> TooManyRequestsException()
                            else -> IOException()
                        }
                        else -> e as Exception
                    }
                )
            }
    }

    override fun getGifById(id: String): Single<GifInfoResult> {
        return service.getGifById(id, API_KEY)
            .map { it.data.first() }
            .map { MapperGifObject.map(it) }
            .map { GifInfoResult(it) }
            .onErrorReturn { e ->
                GifInfoResult(
                    error = when (e) {
                        is HttpException -> when (e.code()) {
                            404 -> NoGifFoundException()
                            429 -> TooManyRequestsException()
                            else -> IOException()
                        }
                        else -> e as Exception
                    }
                )
            }
    }
}

const val API_KEY = "VLyeT79PWYChOntGFeQF6i759XnbABN8"
