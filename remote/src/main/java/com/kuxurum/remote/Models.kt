package com.kuxurum.remote

import com.squareup.moshi.Json

data class GifObjectsData(
    val data: List<GifObject>
)

data class GifObject(
    val id: String,
    val images: Images,
    val user: User?
)

data class Images(
    @Json(name = "fixed_width_downsampled")
    val fixedWidthDownsampled: Image?,
    @Json(name = "fixed_height")
    val fixedHeight: Image?,
    val original: Image
)

data class Image(
    val url: String,
    val width: Int,
    val height: Int
)

data class User(
    @Json(name = "profile_url")
    val profileUrl: String?,
    val username: String?,
    @Json(name = "display_name")
    val displayName: String?,
    val twitter: String?
)