package com.kuxurum.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GiphyApiService {
    @GET("/v1/gifs/trending")
    fun getTrending(@Query("api_key") apiKey: String, @Query("offset") offset: Int): Single<GifObjectsData>

    @GET("/v1/gifs/{id}")
    fun getGifById(@Path("id") id: String, @Query("api_key") apiKey: String): Single<GifObjectsData>
}