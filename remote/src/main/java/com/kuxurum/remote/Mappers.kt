package com.kuxurum.remote

import com.kuxurum.data.EntityGif
import com.kuxurum.data.v
import timber.log.Timber

object MapperGifObject {
    fun map(gifObject: GifObject): EntityGif {
        return EntityGif(
            gifObject.id,
            gifObject.images.fixedHeight?.url ?: gifObject.images.original.url,
            gifObject.images.fixedWidthDownsampled?.url ?: gifObject.images.original.url,
            gifObject.images.fixedWidthDownsampled?.width ?: gifObject.images.original.width,
            gifObject.images.fixedWidthDownsampled?.height ?: gifObject.images.original.height,
            gifObject.user?.profileUrl,
            gifObject.user?.username,
            gifObject.user?.displayName,
            gifObject.user?.twitter
        )
    }
}