plugins {
    id("java")
    kotlin("jvm")
}
dependencies {
    implementation(project(":data"))

    implementation("com.squareup.retrofit2:retrofit:2.5.0")
    implementation("com.squareup.okhttp3:logging-interceptor:3.14.2")
    implementation("com.squareup.retrofit2:converter-moshi:2.5.0")
    implementation("com.squareup.retrofit2:adapter-rxjava2:2.5.0")

    implementation("com.squareup.moshi:moshi:1.8.0")
    implementation("com.squareup.moshi:moshi-kotlin:1.8.0")
}