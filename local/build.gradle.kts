import org.gradle.internal.impldep.com.amazonaws.PredefinedClientConfigurations.defaultConfig
import org.jetbrains.kotlin.kapt3.base.Kapt.kapt

plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
}

android {
    compileSdkVersion(28)

    defaultConfig {
        minSdkVersion(21)
    }
}

dependencies {
    implementation(project(":data"))

    implementation("androidx.room:room-runtime:2.1.0-rc01")
    kapt("androidx.room:room-compiler:2.1.0-rc01")
    implementation("androidx.room:room-rxjava2:2.1.0-rc01")
}