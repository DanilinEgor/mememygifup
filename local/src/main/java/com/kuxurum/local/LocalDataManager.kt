package com.kuxurum.local

import android.content.Context
import androidx.room.Room
import com.kuxurum.data.EntityGif
import com.kuxurum.data.LocalSource
import io.reactivex.Single

class LocalDataManager(context: Context) : LocalSource {
    private val gifDatabase =
        Room.databaseBuilder(context, GifDatabase::class.java, "MemeMyGifUp").build()

    override fun getGifById(id: String): Single<List<EntityGif>> {
        return gifDatabase.gifDao().getById(id)
            .map { it.map { MapperGifObject.mapFromRoom(it) } }
    }

    override fun saveGifs(gifs: List<EntityGif>) {
        gifDatabase.gifDao().insert(gifs.map { MapperGifObject.mapToRoom(it) })
    }
}