package com.kuxurum.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single

@Dao
interface DaoGif {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(gifs: List<RoomEntityGif>)

    @Query("select * from RoomEntityGif where id=:id")
    fun getById(id: String): Single<List<RoomEntityGif>>
}