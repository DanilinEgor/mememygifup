package com.kuxurum.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [RoomEntityGif::class], version = 1)
abstract class GifDatabase : RoomDatabase() {
    abstract fun gifDao(): DaoGif
}