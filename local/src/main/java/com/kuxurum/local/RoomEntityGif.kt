package com.kuxurum.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class RoomEntityGif(
    @PrimaryKey val id: String,
    val url: String,
    val urlDownsampled: String,
    val w: Int,
    val h: Int,
    val profileUrl: String?,
    val username: String?,
    val displayName: String?,
    val twitter: String?
)
