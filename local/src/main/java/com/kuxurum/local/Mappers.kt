package com.kuxurum.local

import com.kuxurum.data.EntityGif

object MapperGifObject {
    fun mapFromRoom(roomEntityGif: RoomEntityGif): EntityGif {
        return EntityGif(
            id = roomEntityGif.id,
            url = roomEntityGif.url,
            urlDownsampled = roomEntityGif.urlDownsampled,
            w = roomEntityGif.w,
            h = roomEntityGif.h,
            profileUrl = roomEntityGif.profileUrl,
            username = roomEntityGif.username,
            displayName = roomEntityGif.displayName,
            twitter = roomEntityGif.twitter
        )
    }

    fun mapToRoom(entityGif: EntityGif): RoomEntityGif {
        return RoomEntityGif(
            id = entityGif.id,
            url = entityGif.url,
            urlDownsampled = entityGif.urlDownsampled,
            w = entityGif.w,
            h = entityGif.h,
            profileUrl = entityGif.profileUrl,
            username = entityGif.username,
            displayName = entityGif.displayName,
            twitter = entityGif.twitter
        )
    }
}